export { default as Avatar } from '../../components/atoms/Avatar.vue'
export { default as Badge } from '../../components/atoms/Badge.vue'
export { default as Breadcrumbs } from '../../components/atoms/Breadcrumbs.vue'
export { default as Btn } from '../../components/atoms/Btn.vue'
export { default as BtnBadge } from '../../components/atoms/BtnBadge.vue'
export { default as BtnDropdown } from '../../components/atoms/BtnDropdown.vue'
export { default as BtnLanguage } from '../../components/atoms/BtnLanguage.vue'
export { default as BtnSection } from '../../components/atoms/BtnSection.vue'
export { default as InputSearch } from '../../components/atoms/InputSearch.vue'
export { default as NotificationBar } from '../../components/atoms/NotificationBar.vue'
export { default as NotificationContainer } from '../../components/atoms/NotificationContainer.vue'
export { default as Card } from '../../components/organisms/Card.vue'
export { default as Footer } from '../../components/organisms/Footer.vue'
export { default as LayoutTiles } from '../../components/organisms/LayoutTiles.vue'
export { default as Navbar } from '../../components/organisms/Navbar.vue'
export { default as Table } from '../../components/organisms/Table.vue'
export { default as TableZotero } from '../../components/organisms/TableZotero.vue'
export { default as Annotation } from '../../components/reader/Annotation.vue'
export { default as BtnSidebarLeft } from '../../components/reader/BtnSidebarLeft.vue'
export { default as BtnSidebarRight } from '../../components/reader/BtnSidebarRight.vue'
export { default as ReaderText } from '../../components/reader/ReaderText.vue'
export { default as Tei } from '../../components/reader/TEI.vue'
export { default as WidgetBibleReferences } from '../../components/reader/WidgetBibleReferences.vue'
export { default as WidgetBibliography } from '../../components/reader/WidgetBibliography.vue'
export { default as WidgetDictionary } from '../../components/reader/WidgetDictionary.vue'
export { default as WidgetIdentifier } from '../../components/reader/WidgetIdentifier.vue'
export { default as WidgetPersons } from '../../components/reader/WidgetPersons.vue'
export { default as WidgetPlaces } from '../../components/reader/WidgetPlaces.vue'
export { default as MenuText } from '../../components/reader/menuText.vue'
export { default as MenuTextAnnotations } from '../../components/reader/menuTextAnnotations.vue'
export { default as MenuTextColumnLeft } from '../../components/reader/menuTextColumnLeft.vue'
export { default as MenuTextColumnRight } from '../../components/reader/menuTextColumnRight.vue'
export { default as MenuTextLeft } from '../../components/reader/menuTextLeft.vue'
export { default as MenuTextPicker } from '../../components/reader/menuTextPicker.vue'
export { default as MenuTextRight } from '../../components/reader/menuTextRight.vue'
export { default as MenuTextToc } from '../../components/reader/menuTextTOC.vue'
export { default as MenuTextTocLeft } from '../../components/reader/menuTextTOCLeft.vue'
export { default as MenuTextTocRight } from '../../components/reader/menuTextTOCRight.vue'
export { default as CardSearch } from '../../components/search/CardSearch.vue'
export { default as SimpleKeyboard } from '../../components/search/SimpleKeyboard.vue'

export const LazyAvatar = import('../../components/atoms/Avatar.vue' /* webpackChunkName: "components/atoms/Avatar'}" */).then(c => c.default || c)
export const LazyBadge = import('../../components/atoms/Badge.vue' /* webpackChunkName: "components/atoms/Badge'}" */).then(c => c.default || c)
export const LazyBreadcrumbs = import('../../components/atoms/Breadcrumbs.vue' /* webpackChunkName: "components/atoms/Breadcrumbs'}" */).then(c => c.default || c)
export const LazyBtn = import('../../components/atoms/Btn.vue' /* webpackChunkName: "components/atoms/Btn'}" */).then(c => c.default || c)
export const LazyBtnBadge = import('../../components/atoms/BtnBadge.vue' /* webpackChunkName: "components/atoms/BtnBadge'}" */).then(c => c.default || c)
export const LazyBtnDropdown = import('../../components/atoms/BtnDropdown.vue' /* webpackChunkName: "components/atoms/BtnDropdown'}" */).then(c => c.default || c)
export const LazyBtnLanguage = import('../../components/atoms/BtnLanguage.vue' /* webpackChunkName: "components/atoms/BtnLanguage'}" */).then(c => c.default || c)
export const LazyBtnSection = import('../../components/atoms/BtnSection.vue' /* webpackChunkName: "components/atoms/BtnSection'}" */).then(c => c.default || c)
export const LazyInputSearch = import('../../components/atoms/InputSearch.vue' /* webpackChunkName: "components/atoms/InputSearch'}" */).then(c => c.default || c)
export const LazyNotificationBar = import('../../components/atoms/NotificationBar.vue' /* webpackChunkName: "components/atoms/NotificationBar'}" */).then(c => c.default || c)
export const LazyNotificationContainer = import('../../components/atoms/NotificationContainer.vue' /* webpackChunkName: "components/atoms/NotificationContainer'}" */).then(c => c.default || c)
export const LazyCard = import('../../components/organisms/Card.vue' /* webpackChunkName: "components/organisms/Card'}" */).then(c => c.default || c)
export const LazyFooter = import('../../components/organisms/Footer.vue' /* webpackChunkName: "components/organisms/Footer'}" */).then(c => c.default || c)
export const LazyLayoutTiles = import('../../components/organisms/LayoutTiles.vue' /* webpackChunkName: "components/organisms/LayoutTiles'}" */).then(c => c.default || c)
export const LazyNavbar = import('../../components/organisms/Navbar.vue' /* webpackChunkName: "components/organisms/Navbar'}" */).then(c => c.default || c)
export const LazyTable = import('../../components/organisms/Table.vue' /* webpackChunkName: "components/organisms/Table'}" */).then(c => c.default || c)
export const LazyTableZotero = import('../../components/organisms/TableZotero.vue' /* webpackChunkName: "components/organisms/TableZotero'}" */).then(c => c.default || c)
export const LazyAnnotation = import('../../components/reader/Annotation.vue' /* webpackChunkName: "components/reader/Annotation'}" */).then(c => c.default || c)
export const LazyBtnSidebarLeft = import('../../components/reader/BtnSidebarLeft.vue' /* webpackChunkName: "components/reader/BtnSidebarLeft'}" */).then(c => c.default || c)
export const LazyBtnSidebarRight = import('../../components/reader/BtnSidebarRight.vue' /* webpackChunkName: "components/reader/BtnSidebarRight'}" */).then(c => c.default || c)
export const LazyReaderText = import('../../components/reader/ReaderText.vue' /* webpackChunkName: "components/reader/ReaderText'}" */).then(c => c.default || c)
export const LazyTei = import('../../components/reader/TEI.vue' /* webpackChunkName: "components/reader/TEI'}" */).then(c => c.default || c)
export const LazyWidgetBibleReferences = import('../../components/reader/WidgetBibleReferences.vue' /* webpackChunkName: "components/reader/WidgetBibleReferences'}" */).then(c => c.default || c)
export const LazyWidgetBibliography = import('../../components/reader/WidgetBibliography.vue' /* webpackChunkName: "components/reader/WidgetBibliography'}" */).then(c => c.default || c)
export const LazyWidgetDictionary = import('../../components/reader/WidgetDictionary.vue' /* webpackChunkName: "components/reader/WidgetDictionary'}" */).then(c => c.default || c)
export const LazyWidgetIdentifier = import('../../components/reader/WidgetIdentifier.vue' /* webpackChunkName: "components/reader/WidgetIdentifier'}" */).then(c => c.default || c)
export const LazyWidgetPersons = import('../../components/reader/WidgetPersons.vue' /* webpackChunkName: "components/reader/WidgetPersons'}" */).then(c => c.default || c)
export const LazyWidgetPlaces = import('../../components/reader/WidgetPlaces.vue' /* webpackChunkName: "components/reader/WidgetPlaces'}" */).then(c => c.default || c)
export const LazyMenuText = import('../../components/reader/menuText.vue' /* webpackChunkName: "components/reader/menuText'}" */).then(c => c.default || c)
export const LazyMenuTextAnnotations = import('../../components/reader/menuTextAnnotations.vue' /* webpackChunkName: "components/reader/menuTextAnnotations'}" */).then(c => c.default || c)
export const LazyMenuTextColumnLeft = import('../../components/reader/menuTextColumnLeft.vue' /* webpackChunkName: "components/reader/menuTextColumnLeft'}" */).then(c => c.default || c)
export const LazyMenuTextColumnRight = import('../../components/reader/menuTextColumnRight.vue' /* webpackChunkName: "components/reader/menuTextColumnRight'}" */).then(c => c.default || c)
export const LazyMenuTextLeft = import('../../components/reader/menuTextLeft.vue' /* webpackChunkName: "components/reader/menuTextLeft'}" */).then(c => c.default || c)
export const LazyMenuTextPicker = import('../../components/reader/menuTextPicker.vue' /* webpackChunkName: "components/reader/menuTextPicker'}" */).then(c => c.default || c)
export const LazyMenuTextRight = import('../../components/reader/menuTextRight.vue' /* webpackChunkName: "components/reader/menuTextRight'}" */).then(c => c.default || c)
export const LazyMenuTextToc = import('../../components/reader/menuTextTOC.vue' /* webpackChunkName: "components/reader/menuTextTOC'}" */).then(c => c.default || c)
export const LazyMenuTextTocLeft = import('../../components/reader/menuTextTOCLeft.vue' /* webpackChunkName: "components/reader/menuTextTOCLeft'}" */).then(c => c.default || c)
export const LazyMenuTextTocRight = import('../../components/reader/menuTextTOCRight.vue' /* webpackChunkName: "components/reader/menuTextTOCRight'}" */).then(c => c.default || c)
export const LazyCardSearch = import('../../components/search/CardSearch.vue' /* webpackChunkName: "components/search/CardSearch'}" */).then(c => c.default || c)
export const LazySimpleKeyboard = import('../../components/search/SimpleKeyboard.vue' /* webpackChunkName: "components/search/SimpleKeyboard'}" */).then(c => c.default || c)
