import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _ffacbf5a = () => interopDefault(import('../pages/imprint/index.vue' /* webpackChunkName: "pages/imprint/index" */))
const _69960617 = () => interopDefault(import('../pages/manuscripts/index.vue' /* webpackChunkName: "pages/manuscripts/index" */))
const _6ef169cf = () => interopDefault(import('../pages/reader/index.vue' /* webpackChunkName: "pages/reader/index" */))
const _7b5b5c74 = () => interopDefault(import('../pages/search/index.vue' /* webpackChunkName: "pages/search/index" */))
const _9f6136a0 = () => interopDefault(import('../pages/texts/index.vue' /* webpackChunkName: "pages/texts/index" */))
const _b65c0224 = () => interopDefault(import('../pages/project/_category.vue' /* webpackChunkName: "pages/project/_category" */))
const _371f5b44 = () => interopDefault(import('../pages/texts/_urn.vue' /* webpackChunkName: "pages/texts/_urn" */))
const _038735d9 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/pta-dev/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/imprint",
    component: _ffacbf5a,
    name: "imprint"
  }, {
    path: "/manuscripts",
    component: _69960617,
    name: "manuscripts"
  }, {
    path: "/reader",
    component: _6ef169cf,
    name: "reader"
  }, {
    path: "/search",
    component: _7b5b5c74,
    name: "search"
  }, {
    path: "/texts",
    component: _9f6136a0,
    name: "texts"
  }, {
    path: "/project/:category?",
    component: _b65c0224,
    name: "project-category"
  }, {
    path: "/texts/:urn",
    component: _371f5b44,
    name: "texts-urn"
  }, {
    path: "/",
    component: _038735d9,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
