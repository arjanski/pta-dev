# Patristic Text Archive
# An open access archive of Late Antique Christian texts 

> The “Patristic Text Archive” provides everyone interested with a collection of Late Antique Christian texts (i.e. “Patristic” is conceived in a very broad sense)

## Development Server & Static Site Generator Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
