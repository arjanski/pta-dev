export const MIRAGE_URL = '/api'
export const BASE_URL =
  'https://xmledit.bbaw.de/ediarum_awma/apps/bibelexegese.web/api'
export const ZOTERO_URL = 'https://api.zotero.org/users/475425'
