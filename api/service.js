/* eslint-disable */
import axios from 'axios'
import { BASE_URL, BBAW_URL, ZOTERO_URL } from './config'

const parseList = (response) => {
  if (response.status !== 200) throw new Error(response.message)
  if (!response.data) return []
  let list = response.data
  if (typeof list !== 'object') {
    list = []
  }
  return list
}

const parseItem = (response, code) => {
  if (response.status !== code) throw new Error(response.message)
  let item = response.data
  if (typeof item !== 'object') {
    item = ''
  }
  return item
}

const getEntities = async function (entityName, format) {
  try {
    const response = await axios.get(`${BASE_URL}/${entityName}`, {
      headers: {
        Accept: `application/${format}`
      }
    })
    const entities = parseList(response, 200)
    return entities
  } catch (error) {
    return error
  }
}

const getEntity = async function (entityName, format) {
  try {
    const response = await axios.get(`${BASE_URL}/pta/${entityName}`, {
      headers: {
        Accept: `application/${format}`
      }
    })
    if (format === 'xml') {
      return response.data
    }
    const entity = parseItem(response, 200)
    return entity
  } catch (error) {
    return error
  }
}

const getZoteroCollections = async function (format) {
  try {
    const response = await axios.get(`${ZOTERO_URL}/collections?v=3`, {
      headers: {
        Accept: `application/${format}`
      }
    })
    const entities = parseList(response, 200)
    return entities
  } catch (error) {
    return error
  }
}

const getZoteroCollection = async function (collectionName, format) {
  try {
    const response = await axios.get(`${ZOTERO_URL}/collections/${collectionName}/items/top?v=3`, {
      headers: {
        Accept: `application/${format}`
      }
    })
    const entity = parseItem(response, 200)
    return entity
  } catch (error) {
    return error
  }
}

const getGithubEntity = async function (entityName) {
  try {
    const response = await axios.get(`https://raw.githubusercontent.com/${entityName}`, {
      headers: {
        Accept: `application/xml`
      }
    })
    return response.data.toString()
  } catch (error) {
    return error
  }
}

export const API = {
  getEntities,
  getEntity,
  getZoteroCollections,
  getZoteroCollection,
  getGithubEntity
}
