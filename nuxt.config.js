import axios from 'axios'

// TODO: Refactoring: $content abfragen statt API!
const dynamicRoutes = async () => {
  // fetching texts
/*     const works = await $content('metadata/cts')
      .where({ 'type' : 'work' })
      .only(['urn'])
      .fetch() */
    

  const resultsTexts = await axios.get(
    'https://xmledit.bbaw.de/ediarum_awma/apps/bibelexegese.web/api/pta?show=list'
  )
  const routesForTexts = resultsTexts.data.map((text) => {
    return {
      route: `/texts/urn:cts:pta:${text.filter.textgroup}.${text.filter.work}`,
      payload: text,
    }
  })

  //concatenating routes
  const routes = routesForTexts
  return routes
}

/* nuxt.config.js */
// only add `router.base = '/<repository-name>/'` if `DEPLOY_ENV` is `GL_PAGES`
const routerBase = process.env.DEPLOY_ENV === 'GL_PAGES' ? {
  router: {
    base: '/pta-dev/'
  }
} : {}

export default {
  // ...routerBase,
  mode: 'spa',
  target: 'static',
  router: {
    base: '/pta-dev/'
  },
  components: true,
  generate: {
    dir: 'public',
    routes: dynamicRoutes,
    fallback: true,
    devtools: true,
  },
  publicRuntimeConfig: {
    baseURL: process.env.BASE_URL || 'https://pta-dev-static.netlify.app'
  },
  privateRuntimeConfig: {},
  /*
   ** Headers of the page
   */
  head: {
    title: 'Patristic Text Archive',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'manifest', href: '/site.webmanifest' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#5bbad5' },
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#B2F5EA' },
  /*
   ** Global CSS
   */
  css: [
    '~/assets/css/inter.css',
    '~/assets/css/v-tooltip.css',
    '~/assets/css/progress-path.css',
    '~/assets/css/literata.css',
    '~/assets/css/CETEIcean.css',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // Doc: https://github.com/Akryum/v-tooltip
    { src: '~/plugins/tooltip', mode: 'client' },
    // Doc: https://github.com/Akryum/vue-progress-path
    '~/plugins/progress-path',
    // Doc: https://franciscohodge.com/projects/simple-keyboard/documentation/
    { src: '~/plugins/simple-keyboard.js', mode: 'client' },
    // Doc: https://miragejs.com/
    // ...(process.env.NODE_ENV !== 'production' ? ['~/plugins/mirage'] : [])
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/components',
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    // Doc: https://github.com/teamnovu/nuxt-breaky
    ['@teamnovu/nuxt-breaky', { enabled: true, position: 'bottomRight' }],
  ],
  tailwindcss: {
    exposeConfig: true,
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxt/content',
    [
      'nuxt-i18n',
      {
        strategy: 'no_prefix',
        locales: [
          { code: 'de', name: 'Deutsch', iso: 'de', file: 'de.json' },
          { code: 'en', name: 'English', iso: 'en', file: 'en.json' },
        ],
        lazy: true,
        langDir: 'languages/',
        defaultLocale: 'en',
        detectBrowserLanguage: {
          // If enabled, a cookie is set once a user has been redirected to his
          // preferred language to prevent subsequent redirections
          // Set to false to redirect every time
          useCookie: true,
          // Set to override the default domain of the cookie. Defaults to host of the site.
          cookieDomain: null,
          // Cookie name
          cookieKey: 'i18n_redirected',
          // Set to always redirect to value stored in the cookie, not just once
          alwaysRedirect: false,
          // If no locale for the browsers locale is a match, use this one as a fallback
          fallbackLocale: 'en',
        },
        vueI18n: {
          fallbackLocale: 'en',
        },
      },
    ],
    '@bazzite/nuxt-optimized-images',
    '@nuxtjs/markdownit',
  ],
  content: {
    fullTextSearchFields: ['content'],
    nestedProperties: []
  },
  // [optional] markdownit options
  // See https://github.com/markdown-it/markdown-it
  markdownit: {
    linkify: true,
    breaks: true
  },
  optimizedImages: {
    optimizeImages: true,
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.resolve.alias['vue'] = 'vue/dist/vue.common'
    },
  },
}
