window.AlpheiosEmbed = (function (e) {
  var t = {}
  function __webpack_require__(i) {
    if (t[i]) return t[i].exports
    var s = (t[i] = { i, l: !1, exports: {} })
    return (
      e[i].call(s.exports, s, s.exports, __webpack_require__),
      (s.l = !0),
      s.exports
    )
  }
  return (
    (__webpack_require__.m = e),
    (__webpack_require__.c = t),
    (__webpack_require__.d = function (e, t, i) {
      __webpack_require__.o(e, t) ||
        Object.defineProperty(e, t, { enumerable: !0, get: i })
    }),
    (__webpack_require__.r = function (e) {
      'undefined' != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: 'Module' }),
        Object.defineProperty(e, '__esModule', { value: !0 })
    }),
    (__webpack_require__.t = function (e, t) {
      if ((1 & t && (e = __webpack_require__(e)), 8 & t)) return e
      if (4 & t && 'object' == typeof e && e && e.__esModule) return e
      var i = Object.create(null)
      if (
        (__webpack_require__.r(i),
        Object.defineProperty(i, 'default', { enumerable: !0, value: e }),
        2 & t && 'string' != typeof e)
      )
        for (var s in e)
          __webpack_require__.d(
            i,
            s,
            function (t) {
              return e[t]
            }.bind(null, s)
          )
      return i
    }),
    (__webpack_require__.n = function (e) {
      var t =
        e && e.__esModule
          ? function getDefault() {
              return e.default
            }
          : function getModuleExports() {
              return e
            }
      return __webpack_require__.d(t, 'a', t), t
    }),
    (__webpack_require__.o = function (e, t) {
      return Object.prototype.hasOwnProperty.call(e, t)
    }),
    (__webpack_require__.p = ''),
    __webpack_require__((__webpack_require__.s = 1))
  )
})([
  function (e) {
    e.exports = JSON.parse(
      '{"c":"3.1.4","a":"45","b":"Alpheios Embedded Library"}'
    )
  },
  function (e, t, i) {
    'use strict'
    i.r(t)
    class State {
      constructor(e) {
        ;(this.panelStatus = void 0),
          (this.tab = void 0),
          (this.watchers = new Map())
      }
      static create(e) {
        let t = new State()
        for (let i of Object.keys(e)) t[i] = e[i]
        return t
      }
      static get defaults() {
        return { panelStatus: State.statuses.panel.OPEN }
      }
      static get statuses() {
        return {
          embedLib: {
            PENDING: Symbol.for('Alpheios_Status_Pending'),
            ACTIVE: Symbol.for('Alpheios_Status_Active'),
            DEACTIVATED: Symbol.for('Alpheios_Status_Deactivated'),
            DISABLED: Symbol.for('Alpheios_Status_Disabled'),
          },
          panel: {
            OPEN: Symbol.for('Alpheios_Status_PanelOpen'),
            CLOSED: Symbol.for('Alpheios_Status_PanelClosed'),
            DEFAULT: Symbol.for('Alpheios_Status_PanelDefault'),
          },
          tab: { DEFAULT: 'default' },
        }
      }
      setWatcher(e, t) {
        return this.watchers.set(e, t), this
      }
      setItem(e, t) {
        return (
          (this[e] = t),
          this.watchers &&
            this.watchers.has(e) &&
            this.watchers.get(e)(e, this),
          this
        )
      }
      isPanelOpen() {
        return this.panelStatus === State.statuses.panel.OPEN
      }
      isPanelClosed() {
        return this.panelStatus === State.statuses.panel.CLOSED
      }
      isPanelStateDefault() {
        return this.panelStatus === State.statuses.panel.DEFAULT
      }
      isPanelStateValid() {
        return (
          this.panelStatus === State.statuses.panel.OPEN ||
          this.panelStatus === State.statuses.panel.CLOSED
        )
      }
      setPanelOpen() {
        return this.setItem('panelStatus', State.statuses.panel.OPEN), this
      }
      setPanelClosed() {
        return this.setItem('panelStatus', State.statuses.panel.CLOSED), this
      }
      changeTab(e) {
        return this.setItem('tab', e), this
      }
      activateUI() {
        return this.setItem('uiActive', !0), this
      }
      isActive() {
        return this.status === State.statuses.embedLib.ACTIVE
      }
      isDeactivated() {
        return this.status === State.statuses.embedLib.DEACTIVATED
      }
      isDisabled() {
        return this.status === State.statuses.embedLib.DISABLED
      }
      isTabStateDefault() {
        return this.tab === State.statuses.tab.DEFAULT
      }
      uiIsActive() {
        return this.uiActive
      }
      activate() {
        return (this.status = State.statuses.embedLib.ACTIVE), this
      }
      deactivate() {
        return (this.status = State.statuses.embedLib.DEACTIVATED), this
      }
      disable() {
        return (this.status = State.statuses.embedLib.DISABLED), this
      }
    }
    var s = i(0)
    class AuthData {
      constructor() {
        ;(this.isAuthenticated = !1),
          (this.accessToken = ''),
          (this.expirationDateTime = new Date(0)),
          (this.hasSessionExpired = !1),
          (this.userId = ''),
          (this.userName = ''),
          (this.userNickname = '')
      }
      erase() {
        ;(this.isAuthenticated = !1),
          (this.accessToken = ''),
          (this.expirationDateTime = new Date(0)),
          (this.hasSessionExpired = !1),
          (this.userId = ''),
          (this.userName = ''),
          (this.userNickname = '')
      }
      static fromSerializable(e) {
        let t = new AuthData()
        return (
          Object.assign(t, e),
          (t.expirationDateTime = e.expirationDateTime
            ? new Date(e.expirationDateTime)
            : void 0),
          t
        )
      }
      serializable() {
        let e = Object.assign({}, this)
        return (e.expirationDateTime = this.expirationDateTime.toJSON()), e
      }
      interopSerializable() {
        let e = Object.assign({}, this)
        return (
          (e.expirationDateTime = Math.round(
            this.expirationDateTime.getTime() / 1e3
          )),
          e
        )
      }
      setAuthStatus(e) {
        return (this.isAuthenticated = e), this
      }
      setSessionDuration(e) {
        return (this.expirationDateTime = new Date(Date.now() + e)), this
      }
      get isSessionActive() {
        return (
          this.isAuthenticated && this.expirationDateTime.getTime() > Date.now()
        )
      }
      get expirationInterval() {
        return this.isSessionActive
          ? this.expirationDateTime.getTime() - Date.now()
          : 0
      }
      expireSession() {
        return this.erase(), (this.hasSessionExpired = !0), this
      }
    }
    class app_authenticator_AppAuthenticator {
      constructor(e) {
        ;(this.auth0Lock = null), (this._auth0profile = null), (this.env = e)
      }
      loginUrl() {
        return null
      }
      logoutUrl() {
        return null
      }
      session() {
        return new Promise((e, t) => {
          t(new Error('Session request not supported'))
        })
      }
      authenticate() {
        return new Promise((e, t) => {
          if (!this.auth0Lock) {
            if (!this.env) {
              let e =
                'Unable to find Auth0 configuration. Auth0 functionality will be disabled'
              console.error(e), t(e)
            }
            if (this.env.TEST_ID) {
              localStorage.setItem('access_token', this.env.TEST_ID),
                localStorage.setItem('id_token', this.env.TEST_ID),
                localStorage.setItem('is_test_user', !0)
              const t = 36e5,
                i = new Date(Date.now() + t)
              localStorage.setItem('expiration_date_time', i.toJSON()),
                e('Authenticated')
            } else
              (this.auth0Lock = new Auth0Lock(
                this.env.CLIENT_ID,
                this.env.DOMAIN,
                {
                  theme: {
                    logo: 'https://alpheios.net/logos/alpheios_32.png',
                    labeledSubmitButton: !1,
                    primaryColor: '#436476',
                  },
                  languageDictionary: {
                    title: 'Login',
                    signUpTerms:
                      'By signing up, you agree to our <a href="https://alpheios.net/pages/userterms" target="_blank">terms of service</a> and <a href="https://alpheios.net/pages/privacy-policy">privacy policy</a>.',
                  },
                  mustAcceptTerms: !0,
                  auth: {
                    redirect: !1,
                    params: {
                      audience: this.env.AUDIENCE,
                      scope: 'openid profile email',
                      prompt: 'consent select_account',
                    },
                    responseType: 'token id_token',
                  },
                }
              )),
                this.auth0Lock.on('authenticated', (t) => {
                  this.auth0Lock.hide(),
                    localStorage.setItem('access_token', t.accessToken),
                    localStorage.setItem('id_token', t.idToken)
                  const i = new Date(Date.now() + 1e3 * t.expiresIn)
                  localStorage.setItem('expiration_date_time', i.toJSON()),
                    e('Authenticated')
                }),
                this.auth0Lock.on('unrecoverable_error', (e) => {
                  console.error('Auth0 Lock unrecoverable error: ', e),
                    t('Auth0 Lock unrecoverable')
                }),
                this.auth0Lock.on('authorization_error', (e) => {
                  console.error('Auth0 Lock authorization error: ', e),
                    t('Auth0Lock authorization error')
                }),
                this.auth0Lock.show()
          }
        })
      }
      getProfileData() {
        return new Promise((e, t) => {
          const i = localStorage.getItem('access_token')
          i ||
            (console.error('You must login to call this protected endpoint!'),
            t('Login required'))
          const s = localStorage.getItem('expiration_date_time')
          let a = new AuthData()
          if (
            (a.setAuthStatus(!0),
            (a.expirationDateTime = new Date(s)),
            localStorage.getItem('is_test_user'))
          ) {
            let t = {
              name: 'Alpheios Test User',
              nickname: 'testuser',
              sub: 'testuser',
            }
            localStorage.setItem('profile', JSON.stringify(t)),
              (a.userId = t.sub),
              (a.userName = t.name),
              (a.userNickname = t.nickname),
              e(a)
          } else
            this.auth0Lock.getUserInfo(i, (i, s) => {
              i
                ? t(i)
                : (localStorage.setItem('profile', JSON.stringify(s)),
                  (a.userId = s.sub),
                  (a.userName = s.name),
                  (a.userNickname = s.nickname),
                  e(a))
            })
        })
      }
      getUserData() {
        return new Promise((e, t) => {
          const i = localStorage.getItem('access_token')
          i ||
            (console.error('You must login to call this protected endpoint!'),
            t('Not Authenticated')),
            e(i)
        })
      }
      getEndPoints() {
        return this.env.ENDPOINTS
      }
      logout() {
        localStorage.removeItem('id_token'),
          localStorage.removeItem('access_token'),
          localStorage.removeItem('profile'),
          this.auth0Lock.logout({ returnTo: this.env.LOGOUT_URL })
      }
    }
    class session_authenticator_SessionAuthenticator {
      constructor(e) {
        ;(this.sessionUrl = e.SESSION_URL),
          (this.tokenUrl = e.TOKEN_URL),
          (this.endpoints = e.ENDPOINTS),
          (this._loginUrl = e.LOGIN_URL),
          (this._logoutUrl = e.LOGOUT_URL),
          (this._authData = new AuthData())
      }
      loginUrl() {
        return this._loginUrl
      }
      logoutUrl() {
        return this._logoutUrl
      }
      session() {
        return new Promise((e, t) => {
          window
            .fetch(this.sessionUrl)
            .then((i) => {
              i.ok
                ? i
                    .json()
                    .then((t) => {
                      this._authData
                        .setAuthStatus(!0)
                        .setSessionDuration(1e3 * t.expires_in),
                        (this._authData.userId = t.sub),
                        (this._authData.userName = t.name),
                        (this._authData.userNickname = t.nickname),
                        e(this._authData)
                    })
                    .catch((e) => {
                      t(
                        new Error(
                          'Unable to decode a session response from a remote server'
                        )
                      )
                    })
                : t(i.code)
            })
            .catch((e) => {
              t(`Session request failed ${e}`)
            })
        })
      }
      authenticate() {
        return new Promise((e, t) => {
          t('Server Side Authenticator')
        })
      }
      getUserData() {
        return new Promise((e, t) => {
          window
            .fetch(this.tokenUrl)
            .then((i) => {
              i.ok ? e(i.json()) : t(i.code)
            })
            .catch((e) => {
              t(`token request failed ${e}`)
            })
        })
      }
      getEndPoints() {
        return this.endpoints
      }
      logout() {
        this._authData.setAuthStatus(!1),
          (this._authData.userId = ''),
          (this._authData.name = ''),
          (this._authData.nickname = '')
      }
    }
    let a
    function importDependencies(e) {
      let t = {}
      switch (e.mode) {
        case 'production':
          t.components = './lib/alpheios-components.min.js'
          break
        case 'development':
          t.components = './lib/alpheios-components.js'
          break
        case 'custom':
          t = e.libs
          break
        case 'cdn':
        default:
          t.components =
            'https://cdn.jsdelivr.net/npm/alpheios-components@latest/dist/alpheios-components.min.js'
      }
      return new Promise((e, i) => {
        let s = [],
          o = import(t.components).then(() => {
            a = window.AlpheiosComponents
          })
        s.push(o),
          Promise.all(s)
            .then(() => {
              e(embedded_Embedded)
            })
            .catch((e) => {
              i(e)
            })
      })
    }
    i.d(t, 'importDependencies', function () {
      return importDependencies
    })
    class embedded_Embedded {
      constructor({
        clientId: e = null,
        authEnv: t = null,
        documentObject: i = document,
        enabledSelector: o = '.alpheios-enabled',
        disabledSelector: r = '',
        enabledClass: n = '',
        disabledClass: l = '',
        mobileTriggerEvent: u = null,
        desktopTriggerEvent: h = null,
        triggerPreCallback: c = (e) => !0,
        popupInitialPos: d = {},
        toolbarInitialPos: p = {},
        actionPanelInitialPos: _ = {},
        layoutType: m = 'default',
        disableTextSelection: b = !1,
        textLangCode: g = null,
        overrideHelp: S = !1,
        simpleMode: f = !1,
      } = {}) {
        if (((this.clientId = e), null === this.clientId))
          throw new Error('Please identify the site.')
        ;(this.doc = i),
          (this.authEnv = t),
          (this.state = new State()),
          (this.enabledSelector = o),
          (this.disabledSelector = r),
          (this.enabledClass = n),
          (this.disabledClass = l),
          (this.desktopTriggerEvent = h),
          (this.mobileTriggerEvent = u),
          (this.triggerPreCallback = c),
          (this.simpleMode = f),
          this.state.setPanelClosed(),
          (this.state.tab = 'info'),
          (this.ui = a.UIController.create(this.state, {
            storageAdapter: a.LocalStorageArea,
            textQueryTriggerDesktop: this.desktopTriggerEvent,
            textQueryTriggerMobile: this.mobileTriggerEvent,
            textQuerySelector: this.enabledSelector,
            triggerPreCallback: this.triggerPreCallback,
            app: { version: `${s.c}.${s.a}`, name: s.b },
            appType: a.Platform.appTypes.EMBEDDED_LIBRARY,
            clientId: this.clientId,
            disableTextSelection: b,
            textLangCode: g,
            overrideHelp: S,
          })),
          this.authEnv
            ? t.CLIENT_ID
              ? this.ui.registerModule(a.AuthModule, {
                  auth: new app_authenticator_AppAuthenticator(t),
                })
              : t.LOGIN_URL &&
                this.ui.registerModule(a.AuthModule, {
                  auth: new session_authenticator_SessionAuthenticator(t),
                })
            : this.ui.registerModule(a.AuthModule, { auth: null })
        let A = {}
        this.simpleMode && (A.showNav = !1),
          this.ui.registerModule(a.PanelModule, A)
        let D = {}
        d &&
          Object.values(d).filter((e) => Boolean(e)).length > 0 &&
          (D.initialPos = d),
          this.simpleMode && (D.showNav = !1),
          this.ui.registerModule(a.PopupModule, D)
        let w = {}
        _ &&
          Object.values(_).filter((e) => Boolean(e)).length > 0 &&
          (w.initialPos = _),
          this.simpleMode ? (w.showNav = !1) : (w.showNav = !0)
        let v = {}
        this.simpleMode && (v.showNav = !1),
          'default' === m
            ? (p &&
                Object.values(p).filter((e) => Boolean(e)).length > 0 &&
                (v.initialPos = p),
              this.ui.registerModule(a.ToolbarModule, v),
              this.ui.registerModule(a.ActionPanelModule, {
                showNav: w.showNav,
              }))
            : 'readingTools' === m &&
              (this.ui.platform.isDesktop
                ? (p &&
                    Object.values(p).filter((e) => Boolean(e)).length > 0 &&
                    (v.initialPos = p),
                  this.ui.registerModule(a.ToolbarModule, v))
                : this.ui.platform.isMobile &&
                  this.ui.registerModule(a.ActionPanelModule, {
                    lookupResultsIn: 'panel',
                    initialPos: w.initialPos,
                    showNav: w.showNav,
                  }))
      }
      get platform() {
        return this.ui.platform
      }
      notifyExtension() {
        this.doc.body.dispatchEvent(new Event('Alpheios_Embedded_Response'))
      }
      async activate() {
        try {
          this.notifyExtension(),
            await this.ui.activate(),
            this.doc.body.setAttribute('alpheios-embed-lib-status', 'active'),
            this.doc.body.addEventListener('Alpheios_Embedded_Check', (e) => {
              this.notifyExtension(e)
            }),
            this.ui.setEmbedLibActive(!0)
        } catch (e) {
          return void console.error(
            `Unexpected error activating Alpheios: ${e}`
          )
        }
        let e = this.enabledSelector
        if (!e) throw new Error('Configuration must define selector')
        let t = this.doc.querySelectorAll(e)
        if (
          (0 === t.length &&
            console.warn(
              `Alpheios was activated for the page but not any content (no elements matching ${t}).`
            ),
          this.enabledClass)
        )
          for (let e of t) e.classList.add(this.enabledClass)
        if (this.disabledSelector) {
          let e = this.doc.querySelectorAll(this.disabledSelector)
          for (let t of e)
            t.setAttribute('data-alpheios-ignore', 'all'),
              this.disabledClass && t.classList.add(this.disabledClass)
        }
        new a.AlignmentSelector(this.doc, {}).activate()
        a.UIController.initAlignedTranslation(
          this.doc,
          '.aligned-translation',
          {
            edges: { left: !0, right: !0, bottom: !1, top: !1 },
            restrictSize: { min: { width: 200 } },
            restrictEdges: { outer: this.doc.body, endOnly: !0 },
            inertia: !0,
          },
          (e) => {
            e.target.style.width = `${e.rect.width}px`
          }
        )
        return this
      }
      openToolbar() {
        this.ui.openToolbar()
      }
      openActionPanel() {
        this.ui.platform.isMobile && this.ui.closePanel(),
          this.ui.openActionPanel()
      }
      closeActionPanel() {
        this.ui.closeActionPanel()
      }
      openActionPanelLookup() {
        this.ui.platform.isMobile && this.ui.closePanel(),
          this.ui.openActionPanel({ showNav: !1 })
      }
      openActionPanelToolbar() {
        this.ui.platform.isMobile && this.ui.closePanel(),
          this.ui.openActionPanel({ showLookup: !1 })
      }
    }
  },
])
