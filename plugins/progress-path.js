// Docs: https://github.com/Akryum/vue-progress-path
// CSS: Classes to be found in @/assets/css/tailwind.css

import Vue from 'vue'
import VueProgress from 'vue-progress-path'

Vue.use(VueProgress, {
  defaultShape: 'circle',
})
