import { Server, Model } from 'miragejs'

export function makeServer({ environment = 'development' } = {}) {
  const server = new Server({
    environment,

    models: {
      textgroup: Model,
      work: Model,
      version: Model,
    },

    seeds(server) {
      server.create('textgroup', {
        imageUrl: 'images/card_placeholder.jpg',
        imageAlt: 'Athanasius of Alexandria',
        imageLicense:
          'Quelle: Johann Rosario @Dribble (https://dribbble.com/shots/3573162-Immanuel-Kant)',
        title: 'Athanasius of Alexandria',
        category: 'PTA0022',
        teaser: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        cts: {
          namespace: 'pta',
          work: 'pta0022',
        },
        path: 'texts/urn:cts:pta:pta0022',
      })
      server.create('textgroup', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense:
          'Quelle: Johann Rosario @Dribble (https://dribbble.com/shots/3510668-Ren-Descartes)',
        title:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        category: 'PTA0100',
        teaser: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        cts: {
          namespace: 'pta',
          work: 'pta0100',
        },
        path: 'texts/urn:cts:pta:pta0100',
      })
      server.create('textgroup', {
        imageUrl: 'images/card_placeholder.jpg',
        imageAlt: '',
        imageLicense:
          'Quelle: Christoph Bernhard Francke: Portrait of Gottfried Wilhelm Leibniz, [CC 0]',
        title: 'Severian of Gabala',
        category: 'PTA0001',
        teaser: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        cts: {
          namespace: 'pta',
          work: 'pta0001',
        },
        path: '#',
      })

      server.create('work', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense: '',
        title:
          'Theologische Erklärung alexandrinischer Kleriker an Alexander von Alexandrien (Urk. 6) ',
        collection:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        manuscriptsCount: 0,
        editions: ['gr', 'lat'],
        translations: ['gr', 'de'],
        cts: {
          namespace: 'pta',
          work: 'pta0100',
          version: 'pta001',
        },
        pta: 'pta0100.pta001',
        path: 'pta0100:pta001',
      })

      server.create('work', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense: '',
        title: 'Document 2',
        collection:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        manuscriptsCount: 0,
        editions: ['gr', 'lat'],
        translations: ['en'],
        cts: {
          namespace: 'pta',
          work: 'pta0100',
          version: 'pta002',
        },
        pta: 'pta0100.pta002',
        path: 'pta0100:pta002',
      })

      server.create('work', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense: '',
        title: 'Document 3',
        collection:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        manuscriptsCount: 0,
        editions: ['gr', 'lat'],
        translations: ['de'],
        cts: {
          namespace: 'pta',
          work: 'pta0100',
          version: 'pta003',
        },
        pta: 'pta0100.pta003',
        path: 'pta0100:pta003',
      })

      server.create('version', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense: '',
        title:
          'Theologische Erklärung alexandrinischer Kleriker an Alexander von Alexandrien (Urk. 6)',
        translations: ['de'],
        textgroup:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        cts: {
          namespace: 'pta',
          work: 'pta0100',
          version: 'pta001.pta-grc1',
        },
      })

      server.create('version', {
        imageUrl: 'images/athanasius.jpg',
        imageAlt: '',
        imageLicense: '',
        title:
          'Theologische Erklärung alexandrinischer Kleriker an Alexander von Alexandrien (Urk. 7)',
        translations: ['de'],
        textgroup:
          'Dokumente zur Geschichte des arianischen Streites (Athanasius Werke III)',
        cts: {
          namespace: 'pta',
          work: 'pta0100',
          version: 'pta001.pta-grc1',
        },
      })
    },

    routes() {
      this.namespace = 'api'

      this.get('/textgroups', (schema) => {
        return schema.textgroups.all()
      }),
        this.get('/works', (schema) => {
          return schema.works.all()
        })
      this.get('/versions', (schema) => {
        return schema.versions.first()
      })
      this.passthrough('https://api.zotero.org/**')
    },
  })

  return server
}
