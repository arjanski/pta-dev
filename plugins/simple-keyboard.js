import Keyboard from 'simple-keyboard'
import 'simple-keyboard/build/css/index.css'

import Vue from 'vue'

Vue.component('simple-keyboard', Keyboard)

Vue.prototype.$Keyboard = Keyboard
