<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://raw.githubusercontent.com/PatristicTextArchive/Schema/master/tei-pta.rng" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title xml:lang="la">De fide et lege naturae (Excerpta in Catena Andreae – Ms Pa)</title>
        <author>
          <persName key="#pta0001">Severianus Gabalensis</persName>
        </author>
        <respStmt>
          <resp>transkribiert von</resp>
          <persName xml:id="AvS">Annette von Stockhausen</persName>
        </respStmt>
      </titleStmt>
      <publicationStmt>
        <authority>Berlin-Brandenburgische Akademie der Wissenschaften</authority>
        <distributor>Patristic Text Archive</distributor>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-sa/4.0/">Available under a Creative Commons Attribution ShareAlike 4.0 International License</licence>
        </availability>
        <date>2018</date>
        <idno type="PTA">pta0001.pta001</idno>
        <idno type="CPG">4185</idno>
        <idno type="BHG"/>
        <idno type="Aldama">399</idno>
        <idno type="Pinakes-Oeuvre">2683</idno>
        <idno type="ClaCla">6ADE02D33A984173A593EA58647E6EE4</idno>
      </publicationStmt>
      <sourceDesc>
        <msDesc>
          <msIdentifier xml:id="Paris_gr_221">
            <settlement>Paris</settlement>
            <repository>Bibliothèque Nationale de France</repository>
            <idno>Grec 221</idno>
            <altIdentifier type="diktyon">
              <idno>49792</idno>
            </altIdentifier>
          </msIdentifier>
        </msDesc>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <refsDecl n="CTS">
        <cRefPattern n="commentary" matchPattern="(.+)" replacementPattern="#xpath(/tei:TEI/tei:text/tei:body/tei:div[@type='transcription']/tei:div[@n='$1'])"/>
      </refsDecl>
    </encodingDesc>
    <profileDesc>
      <langUsage>
        <language ident="grc">Ancient Greek</language>
      </langUsage>
    </profileDesc>
    <revisionDesc>
      <change who="#AvS" when="2017-11-21">Transkription des Textes anhand des digitalen Faksimiles</change>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div type="transcription" xml:lang="grc" n="urn:cts:pta:pta0001.pta001.pta-msCatPa">
        <div type="textpart" subtype="commentary" n="Act:10:4">
          <head>
            <title type="lemma">
              <pb n="61v" facs="http://gallica.bnf.fr/ark:/12148/btv1b10722831m/f65"/>
              <note place="margin">ξγ</note>Σευηριανοῦ ἐπισκόπου Γαβάλων:</title>
          </head>
          <p>Ἐπειδὴ εἶδεν ὁ τῆς ἀληθείας ὀφθαλμὸς· ὁ μέγας
            <lb/>κριτής· ὅτι καλὰ μὲν τὰ ἔργα· νεκρὰ δέ ἐστι πίστιν οὐκ ἔχοντα, ἀποστέλλει
            <lb/>βραβεύοντα τοῖς ἔργοις ἄγγελον·
            <lb/>ὥστε τοὺς καλῶς ἀθλοῦντας στεφανῶσαι
            <lb/>τῆ πίστει. ὡς φησὶν ὁ ἄγγελος· Κορνήλι<add place="above">ε.</add>
            αἱ προσευχαί σου· καὶ αἱ ἐλεημοσύναι
            <lb/>σου, ἀνέβησαν εἰς μνημόσυνον ἐνώπιον τοῦ Θεοῦ· εἰ τοίνυν εἰσηκούσθη ἡ δέ<lb break="no"/>ησις καὶ αἱ ἐλεημοσύναι ἀ<lb break="no"/>νέβησαν, τί μοι τὸ λεῖπον
            <lb/>εἰς δικαιοσύνην· ἀλλα πέμ<lb break="no"/>ψον εἰς ἰόππην· καὶ μετακά<lb break="no"/>λεσαι σίμωνα τὸν ἐπικαλού<lb break="no"/>μενον πέτρον· ὃς ἐλθὼν
            <lb/>λαλήσει σοι ῥήματα ἐν οἷς
            <lb/>σωθήση, σὺ καὶ πᾶς ὁ οἶκός
            <lb/>σου· οὐκ οὖν οὐκ εἶχε τὰ ἔρ<lb break="no"/>γα σωτηρίαν· εἰ γὰρ ἐξ ὧν πέτρος
            <lb/>κηρύσσει σώζεται καὶ αὐτὸς
            <lb/>καὶ ὁ οἶκος, οὐκ εἶχεν οὐδέπω ἐκ τῶν ἔργων τὴν σωτηρίαν· ἕως αὐτῶ τοῖς ἔρ<lb break="no"/>γοις ἐβράβευσεν ἡ πίστις· διὰ τοῦτο καὶ πέτρος ἐλθὼν ἐκ τῆς ἰόππης· καὶ
            <lb/>θεασάμενος τοῦ Θεοῦ τὴν χάριν ἐκχυθεῖσαν ἐν τοῖς τότε νενομισμένοις
            <lb/>ἀλλοφύλοις· ἐπιγνώμων γενόμενος τῆς τοῦ Θεοῦ κρίσεως λέγει· ἐπ’ ἀληθείας κα<lb break="no"/>ταλαμβάνομαι, ὅτι οὐκ ἔστι προσωπολήπτης ὁ Θεός· ἀλλ’ ἐν παντὶ ἔθνει ὁ φοβού<lb break="no"/>μενος αὐτὸν καὶ ἐργαζόμενος δικαιοσύνην σώζεται· ἀλλὰ δεκτός ἐστι·
            <lb/>τουτέστιν ἄξιος γίνεται τοῦ δεχθῆναι· δεῖ τοίνυν προλάμπειν τῶν ἔρ<lb break="no"/>γων τὴν πίστιν, καὶ ἀκολουθεῖν τῆ πίστει τὰ ἔργα· μήτε τὴν πίστιν ὑ<lb break="no"/>βρίζέτω τῆ ἀκαρπία. μήτε τὰ ἔργα ὑβριζέτω τῆ ἀπιστία:-</p>
        </div>
        <div type="textpart" subtype="commentary" n="Act:15:25">
          <head>
            <title type="lemma">
              <pb n="89v" facs="http://gallica.bnf.fr/ark:/12148/btv1b10722831m/f93"/>
              <note place="margin">μθ</note>Σευήρου.</title>
          </head>
          <p>ἔδοξε
            <lb/>τῶ ἁγίω πνεύματι καὶ ἡμῖν· μηδέν σε φησὶν ὁ αἱρετικὸς ἀδελφὲ ξενίση τῶν εἰρημένων·
            <lb/>ὡς ἀδελφόν σε καλεῖ· καὶ ὡς πολέμιον ἀναιρεῖ· μὴ θαυμάσης φησὶν εἰ ὑπὲρ τοῦ ἁγίου
            <lb/>πνεύματος εἰπον οἱ ἀπόστολοι ἔδοξε τῶ ἁγίω πνεύματι· ἀλλ ̓ ὅρα τὸ ἐπαγόμενον· καὶ ἡμῖν γάρ φησιν· ἄρα
            <lb/>οὖν θεοὶ οἱ ἀπόστολοι· ὅτἂν ταῦτα λέγη· ἀπὸ τῆς γραφῆς τὰ βέλη προσφέρομεν·
            <lb/>τί οὖν ἐστὶ πρὸς αὐτοὺς εἰπεῖν· εἰ τὸ συνῆφθαι τοὺς ἀποστόλους τῶ ἁγίω πνεύματι· ἐλατ<lb break="no"/>τοῖ τοῦ πνεύματος τὴν δόξαν. οὐκ οὖν τὸ συνῆφθαι Μωσῆν τῶ Θεῶ. ἐλαττοῖ τοῦ Θεοῦ τὴν
            <lb/>δόξαν· λέγει γὰρ ἡ γραφὴ· ἐπίστευσεν ὁ λαὸς εἰς τὸν Θεὸν καὶ εἰς Μωσῆν· ἀλλὰ καὶ ἕτερον
            <lb/>προσενέγκομεν βέλος· εἰ καθαίρει τοῦ πνεύματος τὴν ἀξίαν συνημμένη ἡ δόξα
            <lb/>τῶν ἀποστόλων· ὑβρίζει καὶ τὸν Θεον· καὶ ἐλαττοῖ αὐτοῦ τὴν ἀξίαν· σαμουὴλ προσκεί<lb break="no"/>μενος· γέγραπται γὰρ· καὶ ἐφοβήθη πᾶς ὁ λαὸς τὸν κύριον καὶ τὸν σαμουήλ·
            <pb n="90r"/>
            <supplied reason="lost">πάλι</supplied>ν καὶ τρίτον ἄλλο προσενέγκωμεν βέλος κατὰ τῆς ἀσεβείας· εἰ ἐλατ<lb/>
            <supplied reason="lost">τοῖ τ</supplied>οῦ ἁγίου πνεύματος τὴν ἀξίαν καὶ τὴν θεϊκὴν αὐθεντείαν· τὸ συνῆφθαι τῶ
            <lb/>
            <supplied reason="lost">πνεύματι</supplied>
            τοὺς ἀποστόλους, ἐλαττοῖ τὴν ἀξίαν τοῦ Θεοῦ γεδεὼν προσκείμενος
            <lb/>
            <supplied reason="lost">αὐ</supplied>τοῦ τῆ προσηγορία· ἐβόησε γὰρ ἅπας ὁ λαὸς, πόλεμος τῶ κυρίω καὶ
            <lb/>
            <supplied reason="lost">τῶ</supplied>
            γεδεών· ὥσπερ οὖν συνῆπται μωϋσῆς τῶ Θεῶ· οὐχ ὡς ὁμότι<lb break="no"/>
            <supplied reason="lost">μ</supplied>ος θεοῦ· ἀλλ’ ὡς ὑπηρέτης θεοῦ· καὶ σαμουὴλ τῶ θεῶ· οὐχ ὡς ὁμότιμος
            <lb/>
            <supplied reason="lost">ἀ</supplied>λλ ̓ ὡς προφήτης· καὶ γεδεὼν τῶ Θεῶ οὐχ ὡς ὁμότιμος· ἀλλ’ ὡς στρα<lb break="no"/>τηγὸς τοῦ πολέμου, οὕτως καὶ οἱ ἀπόστολοι τῶ πνεύματι, ὡς κήρυκες τοῦ
            <lb/>εὐαγγελίου· γνώριζε τοίνυν
            <lb/>τὴν αὐθεντείαν, καὶ μὴ
            <lb/>ὕβριζε τοῦ πνεύματος τὴν ἀξί<lb break="no"/>αν:-
          </p>
        </div>
      </div>
    </body>
  </text>
</TEI>
