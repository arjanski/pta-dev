export const state = () => ({
  navMain: {
    en: [
      {
        name: 'Texts',
        path: 'texts',
      },
      {
        name: 'Manuscripts',
        path: 'manuscripts',
      },
      {
        label: 'Project',
        children: [
          {
            name: 'About the PTA',
            path: 'about',
          },
          {
            name: 'Encoding Guidelines',
            path: 'encoding-guidelines',
          },
          {
            name: 'Contributing',
            path: 'contributing',
          },
          {
            name: 'Contact',
            path: 'contact',
          },
          {
            name: 'Downloads',
            path: 'downloads',
          }
        ],
      }
    ],
    de: [
      {
        name: 'Texte',
        path: 'texts',
      },
      {
        name: 'Handschriften',
        path: 'manuscripts',
      },
      {
        label: 'Projekt',
        children: [
          {
            name: 'Über PTA',
            path: 'about',
          },
          {
            name: 'Encoding Guidelines',
            path: 'encoding-guidelines',
          },
          {
            name: 'Mitwirkung',
            path: 'contributing',
          },
          {
            name: 'Downloads',
            path: 'downloads',
          },
          {
            name: 'Kontakt',
            path: 'contact',
          },
        ],
      }
    ],
  },
})