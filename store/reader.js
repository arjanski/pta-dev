import { API } from '@/api'

export const state = () => ({
  text: {},
  currentEntity: {
    tei: '',
    metadata: {},
    cts: {
      namespace: '',
      textgroup: '',
      work: '',
      version: '',
      urn: '',
    },
    versions: {},
    textgroupTitle: '',
    workTitle: '',
  },
  secondEntity: {
    tei: '',
    metadata: {},
    cts: {
      namespace: '',
      textgroup: '',
      work: '',
      version: '',
      urn: '',
    },
    versions: {
      edition: [],
      translation: [],
      transcription: [],
    },
    textgroupTitle: '',
    workTitle: '',
  },
  textTypes: ['edition', 'praefatio', 'translation', 'transcription'],
  textTypesNew: {
    editions: {},
    translations: {},
    transcriptions: {},
  },
  biblicalReferences: [],
  textTypeLeft: 'edition',
  textTypeRight: 'notext',
  sidebarLeftOpen: true,
  sidebarRightOpen: true,
  menuLeftTextOpen: false,
  menuRightTextOpen: false,
  menuTocLeftOpen: false,
  menuTocRightOpen: false,
  menuBiblicalReferencesOpen: false,
  menuPersonsOpen: false,
  menuPlacesOpen: false,
  widgetBibliographyOpen: true,
  widgetIdentifierOpen: true,
  widgetDictionaryOpen: true,
  widgetBiblicalReferencesOpen: true,
  widgetPersonsOpen: true,
  widgetPlacesOpen: true,
  showBiblicalReferences: true,
  showPersons: true,
  showPlaces: true
})

export const actions = {
  async fetchText({ commit, dispatch }, [urn, side]) {
    const payload = await API.getEntity(urn)
    if (payload instanceof Error) {
      const notification = {
        type: "error",
        source: "fetchText",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      commit('SET_TEXT', [payload, side])
    }
  },
  async fetchBiblicalReferences({ commit, dispatch }) {
    const payload = await API.getGithubEntity('PatristicTextArchive/pta_nemo/master/components/bible-quotations.xml')
    if (payload instanceof Error) {
      const notification = {
        type: "error",
        source: "fetchBiblicalReferences",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      let parser = new DOMParser()
      let doc = parser.parseFromString(payload, "application/xml")
      let references = []
      doc
        .querySelectorAll('reference')
        .forEach(x => {
          references.push({ 'loc': x.getAttribute('loc'), 'content': x.firstChild.textContent })
        });
      commit('SET_BIBLICAL_REFERENCES', references)
    }
  },
  async fetchTEI({ commit, dispatch }, urn) {
    const payload = await API.getEntity(`${urn}?output=xml`, 'xml')
    if (payload instanceof Error) {
      const notification = {
        type: "error",
        source: "fetchTEI",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      commit('SET_TEI_XML', payload)
    }
  }
}

export const mutations = {
  // Set TEI XML
  SET_TEI_XML(state, xml) {
    state.currentEntity.tei = xml
  },
  // Parse and convert XML to array of objects 
  SET_BIBLICAL_REFERENCES(state, references) {
    state.biblicalReferences = references
  },
  // Retrieve CTS components from passed URN string
  SET_CTS_URN(state, urn) {
    let urnSplitted = urn.split('.')
    // Set full URN
    state.currentEntity.cts.urn = urn
    // Set namespace & textgroup
    if (urnSplitted[0]) {
      let splitted = urnSplitted[0].split('-')
      state.currentEntity.cts.textgroup = splitted[3]
      state.currentEntity.cts.namespace = splitted[2]
    }
    // Set work
    if (urnSplitted[1]) {
      state.currentEntity.cts.work = urnSplitted[1]
    }
    // Set version
    if (urnSplitted[2]) {
      state.currentEntity.cts.version = urnSplitted[2]
    }
  },
  SET_TEXT(state, [text, side]) {
    console.log('side set: ', side)
    if (side === 'left') {
      state.currentEntity.label = text.label
      state.currentEntity = Object.assign(state.currentEntity, text)
    }
    if (side === 'right') {
      state.secondEntity.label = text.label
      state.secondEntity = Object.assign(state.secondEntity, text)
    }
  },
  setTextType(state, payload) {
    switch (payload.side) {
      case 'left':
        state.textTypeLeft = payload.type
        break
      case 'right':
        state.textTypeRight = payload.type
        break
    }
  },
  toggleSidebar(state, payload) {
    switch (payload) {
      case 'left':
        state.sidebarLeftOpen = !state.sidebarLeftOpen
        break
      case 'right':
        state.sidebarRightOpen = !state.sidebarRightOpen
    }
  },
  toggleTextMenu(state, payload) {
    switch (payload) {
      case 'left':
        state.menuLeftTextOpen = !state.menuLeftTextOpen
        break
      case 'right':
        state.menuRightTextOpen = !state.menuRightTextOpen
        break
      case 'biblicalReferences':
        state.menuBiblicalReferencesOpen = !state.menuBiblicalReferencesOpen
        break
      case 'persons':
        state.menuPersonsOpen = !state.menuPersonsOpen
        break
      case 'places':
        state.menuPlacesOpen = !state.menuPlacesOpen
        break
    }
  },
  closeTextMenu(state, payload) {
    switch (payload) {
      case 'left':
        state.menuLeftTextOpen = false
        break
      case 'right':
        state.menuRightTextOpen = false
        break
      case 'biblicalReferences':
        state.menuBiblicalReferencesOpen = false
        break
      case 'persons':
        state.menuPersonsOpen = false
        break
      case 'places':
        state.menuPlacesOpen = false
        break
    }
  },
  toggleMenuTOC(state, payload) {
    switch (payload) {
      case 'left':
        state.menuTocLeftOpen = !state.menuTocLeftOpen
        break
      case 'right':
        state.menuTocRightOpen = !state.menuTocRightOpen
        break
    }
  },
  closeMenuTOC(state, payload) {
    switch (payload) {
      case 'left':
        state.menuTocLeftOpen = false
        break
      case 'right':
        state.menuTocRightOpen = false
        break
    }
  },
  setCurrentEntityVersions(state, versions) {
    state.currentEntity.versions = versions
  },
  setTextTypesContent(state, versions) {
    state.textTypesNew.editions = versions.editions
    state.textTypesNew.editions = versions.editions
    state.textTypesNew.editions = versions.editions
  },
  setCurrentEntityMetadata(state, metadata) {
    state.currentEntity.metadata = metadata
  },
  toggleAnnotationRendering(state, payload) {
    switch (payload) {
      case 'biblicalReferences':
        state.showBiblicalReferences = !state.showBiblicalReferences
        break
      case 'persons':
        state.showPersons = !state.showPersons
        break
      case 'places':
        state.showPlaces = !state.showPlaces
        break
    }
  },
  deactivateAllAnnotationRenderings(state) {
    state.showBiblicalReferences = false
    state.showPersons = false
    state.showPlaces = false
  },
  toggleWidgetAccordion(state, payload) {
    switch (payload) {
      case 'bibliography':
        state.widgetBibliographyOpen = !state.widgetBibliographyOpen
        break
      case 'identifier':
        state.widgetIdentifierOpen = !state.widgetIdentifierOpen
        break
      case 'dictionary':
        state.widgetDictionaryOpen = !state.widgetDictionaryOpen
        break
      case 'biblicalReferences':
        state.widgetBiblicalReferencesOpen = !state.widgetBiblicalReferencesOpen
        break
      case 'persons':
        state.widgetPersonsOpen = !state.widgetPersonsOpen
        break
      case 'places':
        state.widgetPlacesOpen = !state.widgetPlacesOpen
        break
    }
  }
}

export const getters = {
  getBibleReference: (state) => (cref) => {
    let ref = state.biblicalReferences.filter(
      (reference) => reference.loc === cref
    )
    return ref[0]
  },
  getCurrentEntityMetadata: (state) => {
    return state.currentEntity.metadata
  }
}
