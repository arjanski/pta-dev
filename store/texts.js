import { API } from '@/api'

export const state = () => ({
  textgroups: [],
  works: [],
  versions: {
    all: [],
    editions: [],
    manuscripts: [],
    translations: [],
  },
  currentEntity: {
    cts: {
      namespace: '',
      textgroup: '',
      work: '',
      version: '',
      urn: '',
    },
    textgroupTitle: '',
    workTitle: '',
  },
})

export const actions = {
  async fetchTextgroups({ commit, dispatch }) {
    const payload = await API.getEntities('pta-textgroup?show=list')
    if (payload instanceof Error) {
      const notification = {
        type: "error",
        source: "fetchTextgroups",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      commit('SET_TEXTGROUPS', payload)
    }
  },
  async fetchWorks({ commit, dispatch }) {
    const payload = await API.getEntities('pta-work?show=list')
    if (payload instanceof Error || payload.length === 0) {
      const notification = {
        type: "error",
        source: "fetchWorks",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      commit('SET_WORKS', payload)
    }
  },
  async fetchVersions({ commit, dispatch }) {
    const payload = await API.getEntities('pta-version?show=list')
    if (payload instanceof Error) {
      const notification = {
        type: "error",
        source: "fetchVersions",
        error: payload
      }
      dispatch('notification/add', notification, { root: true })
    } else {
      commit('SET_VERSIONS', payload)
    }
  },
}

export const mutations = {
  SET_WORKS(state, works) {
    let list = []
    for (let i = 0; i < works.length; i++) {
      // Empty item to be filled and copied to store
      let item = {
        imageUrl: 'images/card_placeholder_new.jpg',
        imageAlt: '',
        imageLicense: '',
        collection: '',
        manuscriptsCount: 0,
        editions: [],
        translations: [],
        title: '',
        cts: {
          namespace: '',
          work: '',
          version: '',
          urn: '',
        },
      }

      // Title
      if (works[i].label) {
        item.title = works[i].label
      }
      // CTS
      if (works[i].id) {
        const cts = works[i].id.split('.')
        // Set namespace & textgroup
        if (cts[0]) {
          let splitted = cts[0].split('-')
          item.cts.namespace = splitted[3].replace(/[0-9]/g, '')
          item.cts.textgroup = splitted[3]
        }
        // Set work
        if (cts[1]) {
          item.cts.work = cts[1]
        }
        // Set urn
        item.cts.urn = works[i].id
      }

      // Manuscripts Count
      if (works[i].filter.edition) {
        let edition = works[i].filter.edition
        let manuscripts = 0
        // If JSON value is an array
        if (typeof edition === 'object') {
          for (const item of edition) {
            if (item.includes('pta-ms')) {
              manuscripts++
            }
          }
          item.manuscriptsCount = manuscripts
        }
      }

      // Editions
      if (works[i].filter.edition) {
        let editions = works[i].filter.edition
        let language = ''
        let languages = []

        // If JSON value is string
        if (typeof editions === 'string') {
          if (editions.includes('pta-grc')) {
            language = 'gr'
          }
          if (editions.includes('pta-deu')) {
            language = 'de'
          }
          if (editions.includes('pta-fra')) {
            language = 'fr'
          }
          if (editions.includes('pta-ita')) {
            language = 'it'
          }
          item.editions = [language]
        }

        // If JSON value is an array
        if (typeof editions === 'object') {
          for (const item of editions) {
            if (item.includes('pta-grc')) {
              languages.push('gr')
            }
            if (item.includes('pta-lat')) {
              languages.push('lat')
            }
            if (item.includes('pta-syr')) {
              languages.push('syr')
            }
          }
          // Remove duplicates & push
          let noDuplicates = [...new Set(languages)]
          item.editions = noDuplicates
        }
      }

      // Translations
      if (works[i].filter.translation) {
        let translation = works[i].filter.translation
        let language = ''
        let languages = []

        // If JSON value is string
        if (typeof translation === 'string') {
          if (translation.includes('pta-grc')) {
            language = 'grc'
          }
          if (translation.includes('pta-deu')) {
            language = 'de'
          }
          if (translation.includes('pta-lat')) {
            language = 'lat'
          }
          if (translation.includes('pta-fra')) {
            language = 'fr'
          }
          if (translation.includes('pta-ita')) {
            language = 'it'
          }
          item.translations = [language]
        }
        // If JSON value is an array
        if (typeof translation === 'array') {
          for (let i; i < translation.length; i++) {
            if (translation[i].includes('pta-grc')) {
              languages.push('grc')
            }
            if (translation[i].includes('pta-deu')) {
              languages.push('de')
            }
          }
          // Remove duplicates & push
          let noDuplicates = [...new Set(languages)]
          item.translations = noDuplicates
        }
      }

      list.push(item)
    }
    state.works = list
  },
  SET_VERSIONS(state, versions) {
    let list = []
    for (let i = 0; i < versions.length; i++) {
      // Empty item to be filled and copied to store
      let item = {
        imageUrl: 'images/card_placeholder_new.jpg',
        imageAlt: '',
        imageLicense: '',
        title: '',
        description: '',
        cts: {
          namespace: '',
          work: '',
          version: '',
          urn: '',
        },
      }

      // Title
      if (versions[i].label) {
        item.title = versions[i].label
      }

      // Description
      if (versions[i].filter.description) {
        item.description = versions[i].filter.description
      }

      // Bibliographic Citation
      if (versions[i].filter.bibliographicCitation) {
        item.bibliographicCitation = versions[i].filter.bibliographicCitation
      }

      // CTS
      if (versions[i].id) {
        const cts = versions[i].id.split('.')
        // Set namespace & textgroup
        if (cts[0]) {
          let splitted = cts[0].split('-')
          item.cts.namespace = splitted[3].replace(/[0-9]/g, '')
          item.cts.textgroup = splitted[3]
        }
        // Set work
        if (cts[1]) {
          item.cts.work = cts[1]
        }
        // Set version
        if (cts[2]) {
          item.cts.version = cts[2]
        }
        // Set urn
        item.cts.urn = versions[i].id
      }
      list.push(item)
    }

    // Sort items by categories (all, editions, manuscripts, translations)
    state.versions.all = list
    state.versions.editions = list.filter(
      (el) =>
        el.cts.version.includes('pta-grc') ||
        el.cts.version.includes('pta-lat') ||
        el.cts.version.includes('pta-syr')
    )
    state.versions.manuscripts = list.filter((el) =>
      el.cts.version.includes('pta-ms')
    )
    state.versions.translations = list.filter(
      (el) =>
        el.cts.version.includes('pta-deu') ||
        el.cts.version.includes('pta-eng') ||
        el.cts.version.includes('pta-fra') ||
        el.cts.version.includes('pta-ita')
    )
  },
  SET_TEXTGROUPS(state, textgroups) {
    let list = []
    for (let i = 0; i < textgroups.length; i++) {
      // Empty item to be filled and copied to store
      let item = {
        imageUrl: 'images/card_placeholder_new.jpg',
        imageAlt: '',
        imageLicense: '',
        collection: '',
        manuscriptsCount: 0,
        editions: [],
        translations: [],
        title: {
          de: '',
          en: '',
          gr: '',
        },
        cts: {
          namespace: '',
          work: '',
          version: '',
          urn: '',
        },
      }

      // Title
      if (textgroups[i].filter?.de) {
        item.title.de = textgroups[i].filter.de
      }
      if (textgroups[i].filter?.en) {
        item.title.en = textgroups[i].filter.en
      }
      if (textgroups[i].filter?.gr) {
        item.title.gr = textgroups[i].filter.gr
      }
      if (textgroups[i].filter?.lat) {
        item.title.lat = textgroups[i].filter.lat
      }
      item.title.fallback = textgroups[i].label

      // Create CTS URN object
      if (textgroups[i].id) {
        const cts = textgroups[i].id.split('.')
        // Set namespace & textgroup
        if (cts[0]) {
          let splitted = cts[0].split('-')
          item.cts.namespace = splitted[3].replace(/[0-9]/g, '')
          item.cts.textgroup = splitted[3]
        }
        item.cts.urn = textgroups[i].id
      }

      list.push(item)
    }
    state.textgroups = list
  },
  // Retrieve CTS components from passed URN string
  SET_CTS_URN(state, urn) {
    let urnSplitted = urn.split('.')
    // Set full URN
    state.currentEntity.cts.urn = urn
    // Set namespace & textgroup
    if (urnSplitted[0]) {
      let splitted = urnSplitted[0].split('-')
      state.currentEntity.cts.textgroup = splitted[3]
      state.currentEntity.cts.namespace = splitted[2]
    }
    // Set work
    if (urnSplitted[1]) {
      state.currentEntity.cts.work = urnSplitted[1]
    }
    // Set version
    if (urnSplitted[2]) {
      state.currentEntity.cts.version = urnSplitted[2]
    }
  },
}

export const getters = {
  getTextgroups: (state) => (urn) => {
    return state.textgroups.filter(
      (textgroup) => textgroup.cts.namespace === urn.namespace
    )
  },
  getTextsForTextgroup: (state) => (urn) => {
    let list = state.works.filter(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup
    )
    return list
  },
  getTextsForWork: (state) => (urn) => {
    let list = {}
    list.all = state.versions.all.filter(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work
    )
    list.editions = state.versions.editions.filter(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work
    )
    list.manuscripts = state.versions.manuscripts.filter(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work
    )
    list.translations = state.versions.translations.filter(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work
    )
    return list
  },
  getTextgroupTitle: (state) => (urn) => {
    let textgroup = state.textgroups.find(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup
    )
    return textgroup.title
  },
  getWorkTitle: (state) => (urn) => {
    let work = state.works.find(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work
    )
    return work.title
  },
  getVersionTitle: (state) => (urn) => {
    let version = state.versions.all.find(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work &&
        text.cts.version === urn.version
    )
    return version.title
  },
  getVersionCitation: (state) => (urn) => {
    let version = state.versions.all.find(
      (text) =>
        text.cts.namespace === urn.namespace &&
        text.cts.textgroup === urn.textgroup &&
        text.cts.work === urn.work &&
        text.cts.version === urn.version
    )
    if (version.bibliographicCitation) return version.bibliographicCitation
    return ''
  },
}
