// ============================================
// states
// ============================================
export const state = () => ({
  vk_visible: false,
  vk_input: null,
})

// ============================================
// mutations
// ============================================
export const mutations = {
  vk_show(state, e) {
    state.vk_input = e.target
    state.vk_visible = true
  },
  vk_hide(state) {
    state.vk_input = null
    state.vk_visible = false
  },
  vk_update_value(state, input) {
    state.vk_input.value = input
    state.vk_input.dispatchEvent(new Event('input', { bubbles: true }))
  },
}
