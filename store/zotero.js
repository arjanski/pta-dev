import { API } from '@/api'

export const state = () => ({ collections: null, selectedCollection: null })

export const actions = {
  async fetchCollections({ commit }) {
    const collections = await API.getZoteroCollections()
    console.log('collections:', collections)
    commit('SET_ZOTERO_COLLECTIONS', collections)
  },
  async fetchCollection({ commit }) {
    const collection = await API.getZoteroCollection('9KH9TNSJ')
    console.log('fetched collection:', collection)
    commit('SET_ZOTERO_COLLECTION', collection)
  },
}

export const mutations = {
  SET_ZOTERO_COLLECTIONS(state, collections) {
    console.log('collections:', collections)
    state.collections = collections
  },
  SET_ZOTERO_COLLECTION(state, collection) {
    console.log('set collection:', collection)
    state.selectedCollection = collection
  },
}

export const getters = {
  // Trim Zotero collection objects
  getCollections: (state) => {
    const collections = state.collections
    let items = []
    for (let i = 0; i < collections.length; i++) {
      let item = {}
      item.data = collections[i].data
      items.push(item)
    }
    return items
  },
}
