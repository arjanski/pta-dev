## Herausgeber

Berlin-Brandenburgische Akademie der Wissenschaften
Jägerstraße 22/23
10117 Berlin

## Vertreter

Prof. Dr. Dr. h. c. mult. Martin Grötschel
Tel.: +49 30 20370-0, E-Mail: bbaw@bbaw.de

## Rechtsform

Rechtsfähige Körperschaft öffentlichen Rechts
Umsatzsteuer-Identifikationsnummer

DE 167 449 058 (gemäß §27 a Umsatzsteuergesetz)

## Technische Realisierung

TELOTA (Berlin-Brandenburgische Akademie der Wissenschaften)
telota@bbaw.de

## Urheberrecht

Die BBAW ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Video-Sequenzen und Texte zu beachten, von ihm selbst erstellte Grafiken, Tondokumente, Video-Sequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Video-Sequenzen und Texte zurückzugreifen.

Alle innerhalb des Internet-Angebotes genannten und gegebenenfalls durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer.

Allein aufgrund der bloßen Nennung ist nicht der Schluß zu ziehen, daß Markenzeichen nicht durch Rechte Dritter geschützt sind.

Das Copyright für veröffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten.

Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Video-Sequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Autors nicht gestattet, wenn es den zulässigen Umfang überschreitet oder den zulässigen Zweck nicht erfüllt.

## Hinweise zum Datenschutz

Diese Website benutzt Matomo, eine Open-Source-Software zur statistischen Auswertung der Besucherzugriffe. Matomo verwendet sog. “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieses Internetangebotes werden auf einem Server der Berlin-Brandenburgischen Akademie der Wissenschaften in Deutschland gespeichert. Die IP-Adresse wird sofort nach der Verarbeitung und vor deren Speicherung anonymisiert.