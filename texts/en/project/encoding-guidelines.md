All files are encoded according to the [PTA-Schema](https://github.com/PatristicTextArchive/Schema).

More information still to be written... Preliminary encoding guidelines (in German only at the moment) in `tei-pta.odd` in  [PTA-Schema](https://github.com/PatristicTextArchive/Schema).

